" disable VI's compatible mode..
set nocompatible

" set encoding=utf-8
set fileencodings=ucs-bom,utf-8
set fileencoding=utf-8
"set encoding=utf-8
set fileformat=unix

" use chinese help
set helplang=cn

" Set mapleader
let mapleader = ","
let g:mapleader = ","

" use chinese menu
" source $VIMRUNTIME/delmenu.vim
" source $VIMRUNTIME/menu.vim

" set gui options
if has("gui_running")
  set guioptions=i
  if has("win32")
    set guifont=Consolas:h9
  else
    set guifont=DejaVu\ Sans\ Mono\ 9
  endif

  let g:zenburn_alternate_Visual = 1
  let g:zenburn_alternate_Include = 1
  let g:zenburn_alternate_Error = 1

  " set color schema
  colorscheme molokai

  " winpos
  set columns=104
  set lines=33
else
  set t_Co=256
  colorscheme molokai
endif

"The commandbar is 2 high
set cmdheight=2

" Enable filetype plugin
filetype plugin on
filetype indent on

" Enable syntax highlight
syntax enable

" Show line number
set nu

" show matching bracets
set showmatch

" Basic editing options
set expandtab
set shiftwidth=2

au FileType xhtml,html,python,vim,javascript setl shiftwidth=2
au FileType python,vim,javascript setl tabstop=2
au FileType xhtml,html setl tabstop=4
au FileType css setl tabstop=2 noexpandtab
au FileType java,php setl shiftwidth=4
au FileType java,php setl tabstop=4

set smarttab
set lbr
set tw=0

set foldcolumn=0
set foldmethod=syntax

"Auto indent
set ai

" Smart indet
set si

" C-style indeting
set cindent

" Wrap lines
set wrap

" Sets how many lines of history VIM har to remember
set history=400

" Set to auto read when a file is changed from the outside
set autoread

" Have the mouse enabled all the time:
set mouse=a

" Do not redraw, when running macros.. lazyredraw
set lz

" set 7 lines to the curors - when moving vertical..
set so=7

" The commandbar is 2 high
set cmdheight=2

" Change buffer - without saving
set hid

" Ignore case when searching
set ignorecase
set incsearch

" Set magic on
set magic

" No sound on errors.
set noerrorbells
set novisualbell
set t_vb=

" How many tenths of a second to blink
set mat=4

" Highlight search things
set hlsearch

" Turn backup off
set nobackup
set nowb
set noswapfile

" smart backspace
set backspace=start,indent,eol

" Set statusline
function! CurrectDir()
    let curdir = substitute(getcwd(), "", "", "g")
    return curdir
endfunction
set statusline=\ [File]\ %F%m%r%h\ %w\ \ [PWD]\ %r%{CurrectDir()}%h\ \ %=[Line]\ %l,%c\ %=\ %P
set laststatus=2

" Actually, the tab does not switch buffers, but my arrows
" Bclose function ca be found in "Buffer related" section
map <leader>bd :Bclose<cr>

" map <down> <leader>bd
" Use the arrows to something usefull
map <leader>, :bn<cr>
map <leader>. :bp<cr>

" Switch to current dir
map <leader>cd :cd %:p:h<cr>

" switch buffers with Tab
map <C-Tab> :bn<CR>

" NERDTree NERDTreeToggle
map <F10> :NERDTreeMirrorToggle<CR>


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Plugin configuration
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" php editing

" remove CR at end of lines
let PHP_removeCRwhenUnix = 1
" Set up automatic formatting
set formatoptions+=tcqlro
" Set maximum text width (for wrapping)
set textwidth=120

let php_folding = 0
let php_strict_blocks = 1
let php_special_vars = 0
let php_special_functions = 0
let php_alt_comparisons = 0
let php_alt_assignByReference = 0
let php_alt_arrays = 0
let php_baselib = 0
let php_oldStyle = 0
let php_fold_heredoc = 0
let php_short_tags = 0
let php_noShortTags = 1

" tagbar
nmap <F8> :TagbarToggle<CR>


" HTML
let g:do_xhtml_mappings = 'yes'
let g:html_tag_case = 'lowercase'
let g:no_html_toolbar = 'yes'
let g:no_html_menu = 'yes'
let g:html_default_charset = 'utf-8'
let g:no_html_tab_mapping = 'yes'

" phpDocumentor for VIM
inoremap <A-/> <ESC>:call PhpDocSingle()<CR>
nnoremap <A-/> :call PhpDocSingle()<CR>
vnoremap <A-/> :call PhpDocRange()<CR>

" set pdv
let g:pdv_cfg_Type = "mixed"
let g:pdv_cfg_Package = ""
let g:pdv_cfg_Version = "$Id$"
let g:pdv_cfg_Author = "vb2005xu <vb2005xu@qq.com>"
let g:pdv_cfg_Copyright = "2014 - 2016 duixueren.net Ltd."
let g:pdv_cfg_License = "New BSD {@link http://www.opensource.org/licenses/bsd-license.php}"
let g:pdv_cfg_php4always = 0
let g:pdv_cfg_php4guess = 0


" bufexplorer
let g:bufExplorerDefaultHelp=1
let g:bufExplorerDetailedHelp=0
let g:bufExplorerSortBy='mru'

nmap <F3> :BufExplorer<CR>


" NERD Commenter
let NERDShutUp = 1


" PHP syntax check
function! PHP_CheckSyntax()
    setlocal makeprg=php\ -l\ -n\ -d\ html_errors=off
    setlocal shellpipe=>

    " Use error format for parsing PHP error output
    setlocal errorformat=%m\ in\ %f\ on\ line\ %l
    make %
endfunction

" Perform :PHP_CheckSyntax()
map <F5> :call PHP_CheckSyntax()<CR>

" Mark
hi MarkWord1  ctermbg=Cyan     ctermfg=Black  guibg=#8CCBEA    guifg=Black
hi MarkWord2  ctermbg=Green    ctermfg=Black  guibg=#A4E57E    guifg=Black
hi MarkWord3  ctermbg=Yellow   ctermfg=Black  guibg=#FFDB72    guifg=Black
hi MarkWord4  ctermbg=Red      ctermfg=Black  guibg=#FF7272    guifg=Black
hi MarkWord5  ctermbg=Magenta  ctermfg=Black  guibg=#FFB3FF    guifg=Black
hi MarkWord6  ctermbg=Blue     ctermfg=Black  guibg=#9999FF    guifg=Black


" vim-php-namespace
inoremap <Leader>u <C-O>:call PhpInsertUse()<CR>
noremap <Leader>u :call PhpInsertUse()<CR>
" inoremap <Leader>e <C-O>:call PhpExpandClass()<CR>
" noremap <Leader>e :call PhpExpandClass()<CR>



" neocomplcache
let g:neocomplcache_enable_at_startup = 1
" Enable omni completion.
autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags

" Enable heavy omni completion.
if !exists('g:neocomplcache_force_omni_patterns')
  let g:neocomplcache_force_omni_patterns = {}
endif
let g:neocomplcache_force_omni_patterns.php = '[^. \t]->\h\w*\|\h\w*::'
let g:neocomplcache_force_omni_patterns.c = '[^.[:digit:] *\t]\%(\.\|->\)'
let g:neocomplcache_force_omni_patterns.cpp = '[^.[:digit:] *\t]\%(\.\|->\)\|\h\w*::'

" For perlomni.vim setting.
" https://github.com/c9s/perlomni.vim
let g:neocomplcache_force_omni_patterns.perl = '\h\w*->\h\w*\|\h\w*::'


" piv - php 5.3 
let g:DisableAutoPHPFolding = 1

" tagbar_phpctags
let g:tagbar_phpctags_memory_limit = '512M'

" custom

if getfsize("vimscript")>0
	source   vimscript
endif
